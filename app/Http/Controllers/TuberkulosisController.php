<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tuberkulosis;

class TuberkulosisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return tuberkulosis::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        //
        $tuberkulosis=new tuberkulosis;
        $tuberkulosis->tahun=$request->tahun;
        $tuberkulosis->kecamatan=$request->kecamatan;
        $tuberkulosis->kasusTB=$request->kasusTB;
        $tuberkulosis->CNR_TB=$request->CNR_TB;
        $tuberkulosis->jmlh_pnddk=$request->jmlh_pnddk;
        $tuberkulosis->target_kasusTB=$request->target_kasusTB;
        $tuberkulosis->angka_kematian=$request->angka_kematian;
        $tuberkulosis->save();

        return "Data Berhasil Masuk";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tuberkulosis  $tuberkulosis
     * @return \Illuminate\Http\Response
     */
    public function show(tuberkulosis $tuberkulosis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tuberkulosis  $tuberkulosis
     * @return \Illuminate\Http\Response
     */
    public function edit(tuberkulosis $tuberkulosis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tuberkulosis  $tuberkulosis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tahun=$request->tahun;
        $kecamatan=$request->kecamatan;
        $kasusTB=$request->kasusTB;
        $CNR_TB=$request->CNR_TB;
        $jmlh_pnddk=$request->jmlh_pnddk;
        $target_kasusTB=$request->target_kasusTB;
        $angka_kematian=$request->angka_kematian;

        $tuberkulosis=tuberkulosis::find($id);
        $tuberkulosis->tahun=$tahun;
        $tuberkulosis->kecamatan=$kecamatan;
        $tuberkulosis->kasusTB=$kasusTB;
        $tuberkulosis->CNR_TB=$CNR_TB;
        $tuberkulosis->jmlh_pnddk=$jmlh_pnddk;
        $tuberkulosis->target_kasusTB=$target_kasusTB;
        $tuberkulosis->angka_kematian=$angka_kematian;
        $tuberkulosis->save();

        return "Data Berhasil di Update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tuberkulosis  $tuberkulosis
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $tuberkulosis=tuberkulosis::find($id);
        $tuberkulosis->delete();

        return "Data Berhasil di Hapus";
    }
}
