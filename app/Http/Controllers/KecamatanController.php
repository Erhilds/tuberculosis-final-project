<?php

namespace App\Http\Controllers;

use App\kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return kecamatan::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        //
        $kecamatan=new kecamatan;
        $kecamatan->nama_kecamatan=$request->nama_kecamatan;
        $kecamatan->longitude=$request->longitude;
        $kecamatan->latitude=$request->latitude;
        $kecamatan->save();

        return "Data Berhasil Masuk";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function show(kecamatan $kecamatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function edit(kecamatan $kecamatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $nama_kecamatan=$request->nama_kecamatan;
        $longitude=$request->longitude;
        $latitude=$request->latitude;

        $kecamatan=kecamatan::find($id);
        $kecamatan->nama_kecamatan=$nama_kecamatan;
        $kecamatan->longitude=$longitude;
        $kecamatan->latitude=$latitude;
        $kecamatan->save();

        return "Data Berhasil di Update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kecamatan  $kecamatan
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $kecamatan=kecamatan::find($id);
        $kecamatan->delete();

        return "Data Berhasil di Hapus";
    }
}
