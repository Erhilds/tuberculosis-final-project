<?php

namespace App\Http\Controllers;

use App\FasKes;
use Illuminate\Http\Request;

class FasKesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return faskes::all();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $faskes=new faskes;
        $faskes->nama_faskes=$request->nama_faskes;
        $faskes->alamat_faskes=$request->alamat_faskes;
        $faskes->save();

        return "Data Berhasil Masuk";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\faskes  $FasKes
     * @return \Illuminate\Http\Response
     */
    public function show(FasKes $faskes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\faskes  $FasKes
     * @return \Illuminate\Http\Response
     */
    public function edit(FasKes $faskes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\faskes  $faskes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $nama_faskes=$request->nama_faskes;
        $alamat_faskes=$request->alamat_faskes;

        $faskes=faskes::find($id);
        $faskes->nama_faskes=$nama_faskes;
        $faskes->alamat_faskes=$alamat_faskes;
        $faskes->save();

        return "Data Berhasil di Update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\faskes  $FasKes
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $faskes=faskes::find($id);
        $faskes->delete();

        return "Data Berhasil di Hapus";
    }
}
