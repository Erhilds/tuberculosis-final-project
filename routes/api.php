<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'APILoginController@login');
Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});

Route::middleware('auth:api')->get('kecamatans', function (Request $request) {
    return $request->kecamatan();
});
Route::get('kecamatan', 'KecamatanController@index');
Route::post('kecamatan', 'KecamatanController@create');
Route::put('kecamatan/{id}', 'KecamatanController@update');
Route::delete('kecamatan/{id}', 'KecamatanController@delete');

Route::middleware('auth:api')->get('tuberkuloses', function (Request $request) {
    return $request->tuberkulosis();
});
Route::get('tuberkulosis', 'TuberkulosisController@index');
Route::post('tuberkulosis', 'TuberkulosisController@create');
Route::put('tuberkulosis/{id}', 'TuberkulosisController@update');
Route::delete('tuberkulosis/{id}', 'TuberkulosisController@delete');

Route::middleware('auth:api')->get('fas_kes', function (Request $request) {
    return $request->faskes();
});
Route::get('faskes', 'FaskesController@index');
Route::post('faskes', 'FaskesController@store');
Route::put('faskes/{id}', 'FaskesController@update');
Route::delete('faskes/{id}', 'FaskesController@delete');
