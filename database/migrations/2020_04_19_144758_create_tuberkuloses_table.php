<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuberkulosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuberkuloses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tahun');
            $table->string('kecamatan');
            $table->string('kasusTB');
            $table->string('CNR_TB');
            $table->string('jmlh_pnddk');
            $table->string('target_kasusTB');
            $table->string('angka_kematian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuberkuloses');
    }
}
